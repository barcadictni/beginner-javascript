/* eslint-disable */
const name = "Hector"; // double quotes you have to escape some characters
const middle = "Daniel"; // double quotes you have to escape some characters
const last = `Sevilla`; // with backticks you dont have to escape characters

// Multiple Lines

/**
 * If you are using single quotes
 * in order to add a multiple line value you have to add back slash at
 * at the end of each line
 */

const song = "This\
\
is\
\
a\
\
multiple line";

/**
 * With backticks you dont need to escape
 *  INTERPOLATION: When you put a variable inside of a string. Using ${variable}
 *  CONCATENATION
 * */

const hello = `Hello my name is ${name}. Nice to meet you`;

// NUMBERS

const age = 29.9;

/**
 * Math.round
 * Math.floor = lower value of a number
 * Math.ceil = upper value of a number
 * Math.random
 * NaN = Non a Number
 */

//  OBJECTS

const person = {
  first: "Hector",
  last: "Sevilla",
  age: 29
};

/**
 * Accesing objects
 * personDOTname = person.name
 */

// NULL AND UNDEFINED

// BOOLEANS
